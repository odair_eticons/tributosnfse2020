<!DOCTYPE html>
<html lang="pt-br">

<!-- odair testou em 18/11/2020 as 21:30 -->
<!-- luciano testando    ksjd fAS -->

<head>
    <title>Principal</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Eticons - Empresa de Tecnologia e Consultoria Publica</title>
    <link href={{ asset('vendor/bootstrap/css/bootstrap.min.css') }} rel="stylesheet">
    <link href={{ asset('vendor/font-awesome/css/font-awesome.min.css')}} rel="stylesheet" type="text/css">
    <link href={{ asset('css/sb-admin.css')}} rel="stylesheet">
    <link href={{ asset('css/estilos.css')}} rel="stylesheet">
    <link href={{ asset('vendor/bootstrap/js/bootstrap.min.js')}} rel="stylesheet">
</head>

<header>
  <h1> <img src="img/logo.png" alt="logo" /> Prefeitura Municipal de Serra Branca</h1>
</header>
<body>
    <div class="container" >
        <div class="row ">
            <div class="col-md-6 col-sm-2">
                <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                        <div class="wrimagecard-topimage_header" style="background-color:rgba(187, 120, 36, 0.1) ">
                            <center><i class="fa fa-address-card" style="color:#BB7824"></i></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                            <h4>Solicitar Acesso
                                <div class="pull-right badge"></div>
                            </h4>
                            Solicitação de Credenciamento para Emissão de Nota Fiscal
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-2">
                <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                        <div class="wrimagecard-topimage_header" style="background-color: rgba(22, 160, 133, 0.1)">
                            <center><i class="fa fa-adn" style="color:#16A085"></i></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                            <h4>Verificar Autenticidade
                                <div class="pull-right badge" id="WrControls"></div>
                            </h4>
                            Verificar autenticiade da Nota Fiscal
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-6 col-sm-2">
                <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                        <div class="wrimagecard-topimage_header" style="background-color:  rgba(213, 15, 37, 0.1)">
                            <center><i class="fa fa-pencil-square-o" style="color:#d50f25"> </i></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                            <h4>Gerar DAM Avulsa
                                <div class="pull-right badge" id="WrForms"></div>
                            </h4>
                            Emitir DAM avulso
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-sm-2">
                <div class="wrimagecard wrimagecard-topimage">
                    <a href="">
                        <div class="wrimagecard-topimage_header" style="background-color:  rgba(51, 105, 232, 0.1)">
                            <center><i class="fa fa-table" style="color:#3369e8"> </i></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                            <h4>Emitir Nota Fiscal Eletrônica
                                <div class="pull-right badge" id="WrGridSystem"></div>
                            </h4>
                            Acessar o sistema de emissão de NFS-E
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>