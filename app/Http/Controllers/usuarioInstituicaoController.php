<?php

namespace App\Http\Controllers;

use App\Models\usuarioInstituicao;
use Illuminate\Http\Request;
use Monolog\Handler\SendGridHandler;


class usuarioInstituicaoController extends Controller
{
   public function create() {
    return view('Principal.index');
    }

   public function logarSistema(request $request)
   {
 
       $login=$request->login;
       $senha=$request->senha;

      // dd($request->login);
       

       $usuario = usuarioInstituicao::where('login', $login)->where('senha', $senha)->first();
       if ($usuario){
          $empresa = $usuario->instituicao()->first();
          if ($empresa){
              
              
                @session_start();
                //dados do usuario
                $_SESSION['login_usuario'] = $usuario->login;
                $_SESSION['nome_usuario']  = $usuario->nome;
                $_SESSION['nivel_usuario'] = $usuario->nivel;
                //dados da instituicao
                $_SESSION['inst_nome']     = $empresa->instituicao;
                $_SESSION['inst_CNPJ']     = $empresa->cnpj;
                $_SESSION['inst_liberada'] = $empresa->liberada;
                
                return view('principal/menuPrincipal');
               
    
            }else{
                echo "<script language='javascript'> window.alert('Dados Incorretos!') </script>";
                return view('index');
            }
        }
   }    
    
        public function logout(){
           @session_start();
           @session_destroy();
           return view('index');
        }
          
    

}
