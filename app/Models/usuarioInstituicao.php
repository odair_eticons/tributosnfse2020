<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class usuarioInstituicao extends Model
{
   protected $table    ='usuariosInstituicao';
   protected $fillable =['nome','cpf','email','login','senha','nivel,','idinstituicao'];
   protected $hidden   =['senha'];    
   protected $timestamp= false;

   public function instituicao() {
      return $this->hasOne(instituicao::class,  'id' , 'idinstituicao');
      } 
}

