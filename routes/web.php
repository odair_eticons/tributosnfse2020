<?php

use App\Http\Controllers\homeController;
use App\Http\Controllers\inicio\telainicialController;
use App\Http\Controllers\instituicaoController;
use App\Http\Controllers\usuarioInstituicaoController;
use App\Models\usuario;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/usuario',     [usuarioInstituicaoController::class,"logarSistema"])->name('logar');
Route::get('/home{id}' ,   [homeController::class,'index']);
Route::get('/inicio' ,     [telainicialController::class,'index'])->name('inicial');



